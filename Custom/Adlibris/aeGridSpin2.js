#target aftereffects
#include "scripts/read_json.js" // jshint ignore:line

// var configFile = 'test.json';
//Focus order 8,9,7,13

var project =  app.project;
var aeImageFolder = "images";
var aetextCompFolder = "textplates";

var aeMasterCompFolder ="Comps";

var config,tempFolder;
buildFile();

function buildFile(){
	var tempFolder = app.project.items.addFolder('temp'); 
	config = getConfig();

	if(config == null){
		$.writeln("no config file");
		return;
	}

	$.writeln(config);

	// updateSliders(config.sliders);
	setTargets()
	
	var targets = getTargets(true);
	var nonTargets = getTargets(false);

	var targetIds = [8,16,7,13];
	var nonTargetIds = [1,2,3,4,5,6,9,10,11,12,14,15,17,18,19,20];

$.writeln('targetIds ',targets.length,'  nonTargetIds ', nonTargets.length);
	importAndReplaceImages(config,targets,targetIds);
	importAndReplaceImages(config,nonTargets,nonTargetIds);

	// scaleImages();
	// updateTextLayers();
	updateColors(config.colors);
	// loadCTA();
	// updateTextLayers()
	// return;

	// var folders = manualImportFiles(tempFolder)
	
	// if(folders == null){
	// 	$.writeln("NO FOLDERS");
	// 	return;
	// }

	// //Get imported folders with images, loop through folder list
	// for (var i = 0; i <folders.length; i++){
	//         var item = folders[i];
	//         $.writeln(item.file);
	//         $.writeln(item.parentFolder);
	//         if ( item instanceof FolderItem ) {
	//         	//Replace footage in project with folder content
	//         	replaceFootage(item)
	//         }
	// }

	tempFolder.remove();	
}
function setTargets(){
	// setVariable("prod-id-1","Slider",5);
	var targets = getTargets();
	// for(i= 0; i<targets.length; i++){
	// 	var target = targets[i];
	// 	var n = i +1;
	// 	setVariable("prod-id-"+n,"Slider",target.position);
	// }

}
function updateSliders(sliders){
	for(i=0; i<sliders.length; i++){
		var slider = config.sliders[i];
		setVariable(slider.name,"Slider",slider.value);
	}
}

function updateColors(colors){
	for(i=0; i<colors.length; i++){
		var color = config.colors[i];
		setVariable(color.name,"Color",color.value);
	}
}

function getConfig(){
	var configFile = File.openDialog("Open the config file");
	$.writeln("configFile");
	$.writeln(configFile);
	$.writeln(configFile.path);

	if(configFile == null){
		$.writeln("no config file");
		return null;
	}
	
	var config = readJSON(configFile);
	config.assetPath = configFile.path;
	return config;
}
function updateTextLayer(layerName, text){
	var controlComp = getControlComp();

	for (var i = 1; i <=controlComp.layers.length; i++){
		layer =  controlComp.layer(i);
		if(layer instanceof TextLayer ){
			$.writeln("found text layer");
			$.writeln(layer.name);
			$.writeln(target);

			if(layer.name == layerName){
				layer.property("Source Text").setValue(target.text1);
			}
	    }
	}		
	
}
function loadCTA(){
	setVariable("cta-type","Slider",config.CTA.type);
	setVariable("cta-text-color","Color",config.CTA.textColor);
	
	updateTextLayer("cta-text-color",config.CTA.text);
	updateTextLayer("cta-url",config.CTA.url);


}
function getTargets(targets){
	var returnTargets = [];
		for(j = 0 ; j < config.containers.length ; j++){
			var container = config.containers[j];
			if(container.featured != undefined && targets){
				returnTargets.push(container);
			}
			if(container.featured == undefined && !targets){
				returnTargets.push(container);
			}
		}
	
	returnTargets.sort(function(a,b){
			return a.featured * 1 - b.featured * 1; 
		});	

	return returnTargets;
}

function getControlComp(){
	for (var i = 1; i <=project.items.length; i++){
	    var item = project.item(i)
	 
	    if(item.parentFolder.name == aeMasterCompFolder && item instanceof CompItem && item.name == '_Controls'){
	    	return item;
	    }
	    
	}
	$.writeln("No controlcomp");
	return null;	
}
function setVariable(name,type,value){
	var controlComp = getControlComp();
	var variableLayer = null;
	if(controlComp == null ) return null;

	for (var i = 1; i <=controlComp.layers.length; i++){
		layer =  controlComp.layer(i);
		$.writeln(layer.name);
		if(layer.name == "Variables" ){
			variableLayer = layer;
		}
	}

	for (var i = 1; i <= variableLayer.Effects.numProperties; i++){
		var effect = variableLayer.effect(i);
		if(effect.name == name){
			$.writeln(value,hexToRgb(value));

			if(type == "Slider") effect(type).setValue(value);
			if(type == "Color")  effect.property("Color").setValue(hexToRgb(value));
			$.writeln(effect(type));
			$.writeln(effect.value);
			
		}
		$.writeln(i, effect.name);
	}

	
}

function updateTextLayer(layerName, text){
	var controlComp = getControlComp();

	for (var i = 1; i <=controlComp.layers.length; i++){
		layer =  controlComp.layer(i);
		if(layer instanceof TextLayer ){
			$.writeln("found text layer");
			// $.writeln(layer.name);
			// $.writeln(target);

			if(layer.name == layerName){
				layer.property("Source Text").setValue(text);
			}
	    }
	}		
	
}
function updateTextLayers(){
	var controlComp = getControlComp();
	$.writeln('controlComp');
	$.writeln(controlComp);
	var layerPrefix = "prod-";
	if(controlComp == null){
		$.writeln("found control Comp");
		return null;
	}
	
	var targetContainers = getTargets(true);
	//Featured products
	for(prod = 0 ; prod < targetContainers.length ; prod++){
		var target = targetContainers[prod];
		for (var i = 1; i <=controlComp.layers.length; i++){
			layer =  controlComp.layer(i);
			if(layer instanceof TextLayer ){
				$.writeln("found text layer");
				$.writeln(layer.name);
				$.writeln(target);

				if(layer.name == layerPrefix + (prod + 1) + "-text-1"){
					// var text = getFeturedText(prod,"text-1");
					layer.property("Source Text").setValue(target.text1);
				}
				if(layer.name == layerPrefix + (prod + 1) + "-text-2"){
					// var text = getFeturedText(prod,"text-2");
					layer.property("Source Text").setValue(target.text2);
				}
		    }
		}		
	}


}
function getFeturedText(n,name){
	for(i = 0 ; i < config.containers.length ; i++){
		var container = config.containers[i];
		if(container.featured == n){
			$.writeln("found container",container[name],name);
			return container[name] != undefined ? container[name] : "undefined";
		}
	}
}
function importAndReplaceImages(config,containers,targetIds){
	var tempFolder = app.project.items.addFolder('temp'); 

	if(project.activeItem != null){
		project.activeItem.selected = false;
	}
	tempFolder.selected = true;
	
	iteratorId = 1;
	for(i=0; i <= containers.length-1;i++){
		var container = containers[i];
		// $.writeln(container.id);

		var folder = new Folder(config.assetPath + "/assets/bilder/");
		if(!folder) break;

		var file = new File(config.assetPath + "/assets/bilder/" + container.id + ".jpg");
		if(!file) continue;

		$.writeln("match ",i," ",container.id);

		if(file.name == container.id + ".jpg"){
			// $.writeln("match ",container.id,file.name);

			try {
		        var importOptions = new ImportOptions (file);                                                                
		        var aeFile = project.importFile (importOptions);
		       	aeFile.parentFolder = tempFolder;
		       	replaceImage(aeFile,targetIds[i],iteratorId);

	       	} catch (error) { alert(error.toString());} 
		}

		// $.writeln("folder file ",file.name);
		// if(folder){
  //           var files = folder.getFiles();
  //           $.writeln("folder ",folder.name," ",files.length);                  	
  //           var count = 1;
  //           for (var j = 0; j < files.length; j++)
  //           {
  //           	var tempfile = files[j];
  //           	// $.writeln("file ",files[j].name.substr(0,1)," ",files[j].name);
  //           	if(tempfile.name == container.id + ".jpg") $.writeln("match ",container.id,files[j].name);
  //           	// if(files[j].name.substr(0,1) != '.'){
	 //            //     try {
	 //            //     	var importOptions = new ImportOptions (files[j]);                                                                
	 //            //     	var file = project.importFile (importOptions);
	 //            //     	file.parentFolder = tempFolder;
	 //            //     	replaceImage(file,container.id,count);
	 //            //     	count++;
	 //            //     } catch (error) { /*alert(error.toString());*/}            		
  //           	// }

          
  //         	}
  //       }

		// project.importFile(new ImportOptions(File(config.assetPath + "/images/" + container.id)));
	}

}
function replaceImage(image,folderName,iterator){
	for (var i = 1; i <=project.items.length; i++){
	    var item = project.item(i)
	   
	    if(item.parentFolder.name == aeImageFolder && item instanceof FolderItem && item.name == folderName){
	    	var folder = item;
	    	// $.writeln("match");
	    	// $.writeln(newFolder.name + " " + folderName);
	    	// $.writeln(newFolder.item(1));
	    	var itemId = iterator;
	    	// if(folder.item(itemId)){
	    		// $.writeln("itemId ",itemId);
	    		// $.writeln("matched iterator",folder.item(itemId).name);
	    		folder.item(itemId).replace(image.file)
	    	// }
	    	// for (var j = 1; j <=newFolder.items.length; j++) {
	    	// 	$.writeln("i " + 1 + ", "+ newFolder.items.length + " " + newFolder.name);
	    	// 	subItem = newFolder.item(j);
	    	// 	// $.writeln("subItem");
	    	// 	// $.writeln(subItem);
	    	// 	if(subItem instanceof FootageItem){
	    	// 		folder.item(j).replace(subItem.file)
	    	// 	}
	    	// }


	        // if(count<files.length && files[count] != undefined){
	        //     item.replace(files[count].file)        
	        //     $.writeln(item.typeName) 
	        //     count++;
	        //     }

	    }
	    
	}	
}
function scaleImages(){
	for (var i = 1; i <=project.items.length; i++){
	    var item = project.item(i)
	   
	    if(item.parentFolder.name == "product_comps" && item instanceof CompItem){

	    	 resizeImage(item);
	    }
	    
	}	
}
function manualImportFiles(tempFolder){

	if(project.activeItem != null){
		project.activeItem.selected = false;
	}
	
	tempFolder.selected = true;
	var files = project.importFileWithDialog();
	return files;
}

function replaceFootage(newFolder){
	$.writeln("replaceFootage:" + newFolder.name);
	// $.writeln(newFolder);
	// $.writeln(aeImageFolder);
	// $.writeln(newFolder.name);
	// $.writeln("replaceFootage2");
	for (var i = 1; i <=project.items.length; i++){
	    var item = project.item(i)
	   
	    if(item.parentFolder.name == aeImageFolder && item instanceof FolderItem && item.name == newFolder.name){
	    	var folder = item;
	    	// $.writeln("match");
	    	// $.writeln(newFolder.name + " " + item.name);
	    	// $.writeln(newFolder.item(1));
	    	for (var j = 1; j <=newFolder.items.length; j++) {
	    		$.writeln("i " + 1 + ", "+ newFolder.items.length + " " + newFolder.name);
	    		subItem = newFolder.item(j);
	    		// $.writeln("subItem");
	    		// $.writeln(subItem);
	    		if(subItem instanceof FootageItem){
	    			folder.item(j).replace(subItem.file)
	    		}
	    	}


	        // if(count<files.length && files[count] != undefined){
	        //     item.replace(files[count].file)        
	        //     $.writeln(item.typeName) 
	        //     count++;
	        //     }

	    }
	    
	}
}
function resizeImage(comp){
    for(var j = 1; j<=comp.numLayers;j++){
        
        var layer = comp.layer(j);

        if(layer.source instanceof FootageItem && layer.hasVideo && !layer.nullLayer && !(layer.source.mainSource instanceof SolidSource)){
            
            // var scaleToWidth = (comp.width*comp.pixelAspect)/(layer.width*layer.source.pixelAspect)*100;
            var scaleToHeight = (comp.height*comp.pixelAspect)/(layer.height*layer.source.pixelAspect)*100;
            scaleToHeight*= 0.9;
            layer.property("Scale").setValue([scaleToHeight,scaleToHeight]);
            layer.property("Position").setValue([comp.width/2,comp.height/2]);
        }

     }
}

// ----------------
// Helper functions
// ----------------

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? [
        parseInt(result[1], 16)/255,
        parseInt(result[2], 16)/255,
        parseInt(result[3], 16)/255
    ] : null;
}


