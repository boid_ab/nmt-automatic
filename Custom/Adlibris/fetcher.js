var http = require('http');
var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
// var sharp = require('sharp');
var EventEmitter = require("events").EventEmitter;
var events = new EventEmitter();
var path = require('path');
var url = require("url");
var fileUrls = [
                  'http://s2.adlibris.com/images/28066541/syndafloder.jpg',
                  'http://s2.adlibris.com/images/28065885/dar-satan-har-sin-tron.jpg',
                  'http://s1.adlibris.com/images/27837919/torst.jpg',
                  'http://s1.adlibris.com/images/27847220/inland.jpg',
                  'http://s2.adlibris.com/images/28020057/haxan.jpg',
                  'http://s1.adlibris.com/images/29270264/selfies.jpg',
                  'http://s2.adlibris.com/images/27818828/angivaren.jpg',
                  'http://s1.adlibris.com/images/27813536/fixaren-morden-i-hudiksvall.jpg',
                  'http://s1.adlibris.com/images/27830875/elddopt.jpg',
                  'http://s2.adlibris.com/images/27845487/det-du-inte-vet.jpg',
                  'http://s2.adlibris.com/images/27855354/speglarnas-bok.jpg',
                  'http://s2.adlibris.com/images/27861328/den-enkla-sanningen.jpg',
                  'http://s2.adlibris.com/images/27971897/fore-fallet.jpg',
                  'http://s1.adlibris.com/images/27971900/den-hemliga-kvinnan.jpg',
                  'http://s1.adlibris.com/images/27923230/den-sovande-spionen.jpg',
                  'http://s1.adlibris.com/images/27861724/jag-fann-dig.jpg',
                  'http://s1.adlibris.com/images/27808152/eldpojken.jpg',
                  'http://s2.adlibris.com/images/27813184/flickan-i-glastornet.jpg',
                  'http://s2.adlibris.com/images/27816256/nasselvrede.jpg',
                  'http://s2.adlibris.com/images/28228162/kolibri-anna-fekete-1.jpg',
                  'http://s2.adlibris.com/images/28104439/ett-langt-spar-av-blod.jpg'
              ]
var ISBNbase = "http://bilder.fsys.se/";
var ISBNlist = [
  9789155264123,
  9789163614859,
  9789174692143,
  9789172998650,
  9789129703481,
  9789176631874,
  9789188279347,
  9789176212264,
  9789132178672,
  32198540,
  32198045,
  9789163888649,
  9789163895890,
  9789163895999,
  9789150222371,
  9789150117943,
  9789150119411,
  9789129704273,
  9789176631812,
  9789157029904,
  32198106,
  32198069,
  9789163889950,
  9789163896330,
  9789163894589,
  9789150222173,
  9789150222340,
  9789150221923,
  9789150119381,
  9789150119305,
  9789150119695,
  9789150119756,
  9789150119329,
  9789150119169,
  9789150119350
]

module.exports = {
  run: function () {
    console.log("run");
    start();
  },
  moveFile: function (file) {
    console.log("move",file);
    var dir = path.dirname(file);
    var fileName = path.basename(file);

    var newDir = dir+'/selected/'
    if (!fs.existsSync(newDir)){
        fs.mkdirSync(newDir);
    }
    fs.createReadStream(file).pipe(fs.createWriteStream(newDir+fileName));
    console.log("newDir",newDir,fileName);
  },
  events:events
};
// http://www.tradera.com/designersmycken-c3_302640?itemType=Auction&sortBy=MostBids
// http://www.tradera.com/exklusiva-klockor-c3_1901?itemType=Auction&sortBy=MostBids
// http://www.tradera.com/smink-c3_302685?itemType=Auction&sortBy=MostBids
// http://www.tradera.com/skor-1623?itemType=Auction&sortBy=MostBids&subDepartments=Damskor%3bHerrskor&shoeTypes=Sportskor
// http://www.tradera.com/skor-1623?itemType=Auction&sortBy=MostBids&subDepartments=Damskor%3bHerrskor&shoeTypes=Sportskor
var resultObj = [];
var category = 'deckare';
// var fullpath = '/skor-1623?itemType=Auction&sortBy=MostBids&subDepartments=Damskor%3bHerrskor&shoeTypes=Sportskor';
// var fullpath =  '/' + category;

var options = {
  host: 'http://www.tradera.com',
  path: "",
};

function downloadFile(url, fileName){
  var dir = "data/"+category+"/";
  if (!fs.existsSync('data')){
        fs.mkdirSync('data');
  }

  if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
  }
  var fullFilePath = dir+fileName+".jpg";
  var file = fs.createWriteStream(fullFilePath);

  return new Promise(function(resolve, reject) {

    
    file.on('finish', function() {
      console.log("file finished");
      events.emit("data",{fileName:fileName,dir:dir});
      // resizeImage(fileName,dir);
      resolve(fullFilePath);
    });


    var req = http.get(url, function(response) {
      console.log("downloaded",fileName);
      response.pipe(file);
    });
    req.on('error', function(e) {
      reject('problem with request: ' + e.message);
      // console.log('problem with request: ' + e.message);
    });
    req.end(); 
    // // do a thing, possibly async, then…

    // if (/* everything turned out fine */) {
    //   resolve("Stuff worked!");
    // }
    // else {
    //   reject(Error("It broke"));
    // }
  });

}
function getProductPage(url){
  resultObj.push({url:url,imgs:[]});
  var urlResults = resultObj[resultObj.length-1];
  request(url, function (error, response, html) {
    if (!error && response.statusCode == 200) {
      console.log("got",url);
      // console.log(html);
      var $ = cheerio.load(html);
      var title =  $('.view-item-details-header h1')[0].children[0].data;
      var bid = $('.view-item-bidding-details-amount span')[0].children[0].data.replace(/\s/g, '');
      var id = $(".view-item-bidding-form input[name='id']")[0].attribs.value;
      console.log("id",id);
      title = title.replace(/,|:|\//g, ' ');
      // title = title.replace(/\,/g, ' ');
      // title = title.replace(/\:/g, '');
      
      console.log("title",title);
      $('.image-gallery-item img').each(function(i, element){
        // var img = $(this).prev();
       
        // imgUrl = element.attribs.src.substring(2, element.attribs.src.length);
        // downloadFile(imgUrl,element.attribs.alt);
        if(element.attribs['data-image-normal'] != undefined){
            
          imgUrl = "http://"+element.attribs['data-image-normal'].substring(2, element.attribs['data-image-normal'].length);
          fileName = id +"--"+i+"--"+ title + "--" + bid;
          downloadFile(imgUrl,fileName).then(function(result){
            urlResults.imgs.push(result);
             events.emit("update",resultObj);
          },
          function(error){
            console.log(error);
          })
          console.log(fileName);
        }
        

      });

    }
  });
}
function resizeImage(fileName,filePath){
  console.log("resize",fileName);
  resizePath = filePath+"resize/";
  if (!fs.existsSync(resizePath)){
      fs.mkdirSync(resizePath);
  }

  sharp(filePath + fileName + ".jpg")
    .resize(1920)
    .toFile(resizePath + fileName + "--resize.jpg", function(err) {
      // output.jpg is a 300 pixels wide and 200 pixels high image
      // containing a scaled and cropped version of input.jpg
      console.log(err);
    });
}
function start(){
  var resultObj = [];
  fileUrls.forEach(function(imgUrl){
    var parsed = url.parse(imgUrl);

    var fileName = path.basename(parsed.pathname,'.jpg');

    downloadFile(imgUrl,fileName).then(function(result){
      // urlResults.imgs.push(result);
      resultObj.push({url:imgUrl,imgs:[result]});
       events.emit("update",resultObj);
    },
    function(error){
      console.log(error);
    })
  })


}

function fetchISBN(){
  var resultObj = [];
  ISBNlist.forEach(function(ISBN){
    var imgUrl = ISBNbase + ISBN + ".jpg";
    var parsed = url.parse(imgUrl);

    var fileName = path.basename(parsed.pathname,'.jpg');
    console.log(fileName);
    
    downloadFile(imgUrl,fileName).then(function(result){
      // urlResults.imgs.push(result);
      resultObj.push({url:imgUrl,imgs:[result]});
       events.emit("update",resultObj);
    },
    function(error){
      console.log(error);
    })
  })


}

fetchISBN();

// var req = http.request(options, function(res) {
//   console.log('STATUS: ' + res.statusCode);
//   console.log('HEADERS: ' + JSON.stringify(res.headers));
//   res.setEncoding('utf8');
//   res.on('data', function (chunk) {
//     console.log('BODY: ' + chunk);
//   });
// });

// req.on('error', function(e) {
//   console.log('problem with request: ' + e.message);
// });

// // write data to request body
// // req.write('data\n');
// // req.write('data\n');
// req.end();