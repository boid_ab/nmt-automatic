var http = require('http');
var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var sharp = require('sharp');
var EventEmitter = require("events").EventEmitter;
var events = new EventEmitter();
var path = require('path');

module.exports = {
  run: function () {
    console.log("run");
    start();
  },
  moveFile: function (file) {
    console.log("move",file);
    var dir = path.dirname(file);
    var fileName = path.basename(file);

    var newDir = dir+'/selected/'
    if (!fs.existsSync(newDir)){
        fs.mkdirSync(newDir);
    }
    fs.createReadStream(file).pipe(fs.createWriteStream(newDir+fileName));
    console.log("newDir",newDir,fileName);
  },
  events:events
};
// http://www.tradera.com/designersmycken-c3_302640?itemType=Auction&sortBy=MostBids
// http://www.tradera.com/exklusiva-klockor-c3_1901?itemType=Auction&sortBy=MostBids
// http://www.tradera.com/smink-c3_302685?itemType=Auction&sortBy=MostBids
// http://www.tradera.com/skor-1623?itemType=Auction&sortBy=MostBids&subDepartments=Damskor%3bHerrskor&shoeTypes=Sportskor
// http://www.tradera.com/skor-1623?itemType=Auction&sortBy=MostBids&subDepartments=Damskor%3bHerrskor&shoeTypes=Sportskor
var resultObj = [];
var category = 'sportskor';
var fullpath = '/skor-1623?itemType=Auction&sortBy=MostBids&subDepartments=Damskor%3bHerrskor&shoeTypes=Sportskor';
// var fullpath =  '/' + category;

var options = {
  host: 'http://www.tradera.com',
  path: fullpath,
};

function downloadFile(url, fileName){
  var dir = "data/"+category+"/";

  if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
  }
  var fullFilePath = dir+fileName+".jpg";
  var file = fs.createWriteStream(fullFilePath);

  return new Promise(function(resolve, reject) {

    
    file.on('finish', function() {
      console.log("file finished");
      events.emit("data",{fileName:fileName,dir:dir});
      // resizeImage(fileName,dir);
      resolve(fullFilePath);
    });


    var req = http.get(url, function(response) {
      console.log("downloaded",fileName);
      response.pipe(file);
    });
    req.on('error', function(e) {
      reject('problem with request: ' + e.message);
      // console.log('problem with request: ' + e.message);
    });
    req.end(); 
    // // do a thing, possibly async, then…

    // if (/* everything turned out fine */) {
    //   resolve("Stuff worked!");
    // }
    // else {
    //   reject(Error("It broke"));
    // }
  });

}
function getProductPage(url){
  resultObj.push({url:url,imgs:[]});
  var urlResults = resultObj[resultObj.length-1];
  request(url, function (error, response, html) {
    if (!error && response.statusCode == 200) {
      console.log("got",url);
      // console.log(html);
      var $ = cheerio.load(html);
      var title =  $('.view-item-details-header h1')[0].children[0].data;
      var bid = $('.view-item-bidding-details-amount span')[0].children[0].data.replace(/\s/g, '');
      var id = $(".view-item-bidding-form input[name='id']")[0].attribs.value;
      console.log("id",id);
      title = title.replace(/,|:|\//g, ' ');
      // title = title.replace(/\,/g, ' ');
      // title = title.replace(/\:/g, '');
      
      console.log("title",title);
      $('.image-gallery-item img').each(function(i, element){
        // var img = $(this).prev();
       
        // imgUrl = element.attribs.src.substring(2, element.attribs.src.length);
        // downloadFile(imgUrl,element.attribs.alt);
        if(element.attribs['data-image-normal'] != undefined){
            
          imgUrl = "http://"+element.attribs['data-image-normal'].substring(2, element.attribs['data-image-normal'].length);
          fileName = id +"--"+i+"--"+ title + "--" + bid;
          downloadFile(imgUrl,fileName).then(function(result){
            urlResults.imgs.push(result);
             events.emit("update",resultObj);
          },
          function(error){
            console.log(error);
          })
          console.log(fileName);
        }
        

      });

    }
  });
}
function resizeImage(fileName,filePath){
  console.log("resize",fileName);
  resizePath = filePath+"resize/";
  if (!fs.existsSync(resizePath)){
      fs.mkdirSync(resizePath);
  }

  sharp(filePath + fileName + ".jpg")
    .resize(1920)
    .toFile(resizePath + fileName + "--resize.jpg", function(err) {
      // output.jpg is a 300 pixels wide and 200 pixels high image
      // containing a scaled and cropped version of input.jpg
      console.log(err);
    });
}
function start(){
    var resultObj = [];
   request(options.host+options.path, function (error, response, html) {
    if (!error && response.statusCode == 200) {
      // console.log(html);
      var $ = cheerio.load(html);
      $('.item-card-figure a').each(function(i, element){
        // var img = $(this).prev();
       
        // imgUrl = element.attribs.src.substring(2, element.attribs.src.length);
        // downloadFile(imgUrl,element.attribs.alt);
        console.log("fetch",options.host+element.attribs.href);
        
        productUrl = options.host+element.attribs.href;
        // if(i < 8){
          getProductPage(productUrl);
        // }
        
      });

    }
  }); 
}




// var req = http.request(options, function(res) {
//   console.log('STATUS: ' + res.statusCode);
//   console.log('HEADERS: ' + JSON.stringify(res.headers));
//   res.setEncoding('utf8');
//   res.on('data', function (chunk) {
//     console.log('BODY: ' + chunk);
//   });
// });

// req.on('error', function(e) {
//   console.log('problem with request: ' + e.message);
// });

// // write data to request body
// // req.write('data\n');
// // req.write('data\n');
// req.end();